package model;

import java.util.ArrayList;

public class TodoList {
    private ArrayList<Task> taskList = new ArrayList<>();

    public TodoList() {
        this.taskList.add(new Task(1, "Go to church", Status.DONE, "Rohani"));
        this.taskList.add(new Task(2, "Coding", Status.NOT_DONE, "Kuliah"));
        this.taskList.add(new Task(3, "Assignment", Status.DONE, "Kuliah"));
    }

    public void addNewTaskByUser(Task newTask) {
        taskList.add(newTask);
    }

    private Task findTaskById(int id){
        for (Task task : taskList) {
            if (task.getId() == id) {
                return task;
            }
        }
        return null;
    }

    public String getTaskById(int id) {
        Task task = findTaskById(id);
        if (task != null) return task.getTask();
        else return "invalid input";
    }

    public void markTaskToDone(int id) {
        Task task = findTaskById(id);
        if (task != null) task.setStatus(Status.DONE);
    }

    public String getAllTodoList() {
        StringBuilder allTaskList = new StringBuilder();
        Task lastTask = taskList.get(taskList.size() - 1);

        for (Task task : taskList) {
            allTaskList.append(task.getTask());
            if (task != lastTask) {
                allTaskList.append("\n");
            }
        }

        return allTaskList.toString();
    }

    public String getTaskListByCategory(String category) {
        StringBuilder allTaskList = new StringBuilder();
        Task lastTask = taskList.get(taskList.size() - 1);

        for (Task task : taskList) {
            if (task.getCategory().equals(category)) {
                allTaskList.append(task.getTask());
                if (task != lastTask) {
                    allTaskList.append("\n");
                }
            }
        }

        return allTaskList.toString();
    }


    public void deleteTaskById(int id) {
        for (int i = 0; i < taskList.size(); i++) {
            int taskId = taskList.get(i).getId();

            if (taskId == id) {
                taskList.remove(i);
                return;
            }
        }
    }
}
