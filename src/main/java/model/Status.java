package model;

public enum Status {
    DONE("[DONE]"),
    NOT_DONE("[NOT DONE]");

    private String status;

    Status(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
