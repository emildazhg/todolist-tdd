package model;

public class Task {

    private int id;
    private String name;
    private Status status;
    private String category;

    public Task(int id, String name, Status status, String category) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.category = category;
    }

    public String getTask() {
        return getId() + ". " + getName() + " " + getStatus();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

}
