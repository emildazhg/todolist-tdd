package menu;

import model.Status;
import model.Task;
import model.TodoList;

import java.util.Scanner;

public class Display {
    private static TodoList todoList = new TodoList();
    private static Scanner scan = new Scanner(System.in);
    private int id;
    private String category;

    private static void Menu() {
        System.out.println("TodoList");
        System.out.println("=====================================");
        System.out.println("1. See all task");
        System.out.println("2. Get task by ID");
        System.out.println("3. Update Status");
        System.out.println("4. Search task by category");
        System.out.println("5. Add new task");
        System.out.println("6. Delete task by ID");
        System.out.println("0. Exit");
        System.out.print("Choose > ");
    }

    public void display() {

        int choice;
        do {
            Menu();
            choice = scan.nextInt();

            switch (choice) {
                case 1:
                    displayAllTodoList();
                    break;
                case 2:
                    displayTaskById();
                    break;
                case 3:
                    selectTaskToBeUpdated();
                    break;
                case 4:
                    displayTaskByCategory();
                    break;
                case 5:
                    insertNewTask();
                    break;
                case 6:
                    deleteSelectedTask();
                    break;
                case 0:
                    break;
                default:
                    handleDefaultInput();
            }
        } while (choice != 0);
    }

    private void displayAllTodoList() {
        System.out.println(todoList.getAllTodoList() + "\n");
    }

    private void displayTaskById() {
        System.out.print("Input task id to be printed: ");

        id = scan.nextInt();

        System.out.println(todoList.getTaskById(id) + "\n");
    }

    private void selectTaskToBeUpdated() {
        System.out.println(todoList.getAllTodoList());
        System.out.print("Input task id to be updated:");

        id = scan.nextInt();

        todoList.markTaskToDone(id);

        System.out.println("Result: ");
        System.out.println(todoList.getAllTodoList() + "\n");
    }

    private void displayTaskByCategory() {
        System.out.println("Input category to be searched:");
        scan.nextLine();
        category = scan.nextLine();

        System.out.println(todoList.getTaskListByCategory(category) + "\n");
    }

    private void insertNewTask() {
        System.out.println("Input new ID:");
        id = scan.nextInt();
        scan.nextLine();

        System.out.println("Input new Name:");
        String name = scan.nextLine();

        System.out.println("Input Category:");
        category = scan.nextLine();

        todoList.addNewTaskByUser(new Task(id, name, Status.NOT_DONE, category));
    }

    private void deleteSelectedTask() {
        System.out.println("Input ID to be deleted:");
        id = scan.nextInt();

        todoList.deleteTaskById(id);
    }

    private void handleDefaultInput() {
        System.out.println("Invalid Input");
    }
}
