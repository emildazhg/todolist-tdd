import menu.Display;

public class Main {
    private static Display display = new Display();

    public static void main(String[] args) {
        display.display();
    }
}
