package model;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void getTaskTest() {
        Task task = new Task(1, "Go to Church", Status.DONE, "Rohani");
        String expectedResult = "1. Go to Church [DONE]";

        assertEquals(expectedResult, task.getTask());
    }

    @Test
    public void getTest() {
        Task task = new Task(1, "Go to Church", Status.DONE, "Rohani");
        int expectedId = 1;
        String expectedName = "Go to Church";
        Status expectedStatus = Status.DONE;
        String expectedCategory = "Rohani";

        assertEquals(expectedId, task.getId());
        assertEquals(expectedName, task.getName());
        assertEquals(expectedStatus, task.getStatus());
        assertEquals(expectedCategory, task.getCategory());
    }


    @Test
    public void updateTaskStatusTest() {
        Task task = new Task(1, "Go to Church", Status.NOT_DONE, "Rohani");
        Status expected = Status.DONE;

        task.setStatus(Status.DONE);

        assertEquals(expected, task.getStatus());
    }
}