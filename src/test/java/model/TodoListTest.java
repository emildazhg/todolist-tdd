package model;

import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {
    @Test
    public void addNewTaskTest() {
        TodoList taskList = new TodoList();
        String expectedResult = "14. Cooking [NOT DONE]";

        Task newTask = new Task(14, "Cooking", Status.NOT_DONE, "Housework");
        taskList.addNewTaskByUser(newTask);

        assertEquals(expectedResult, taskList.getTaskById(14));
    }

    @Test
    public void getTasksByIdTest() {
        TodoList taskList = new TodoList();
        String expectedResult = "1. Go to church [DONE]";

        String actual = taskList.getTaskById(1);

        assertEquals(expectedResult, actual);
    }

    @Test
    public void getTasksByIdTestReturnNullTest() {
        TodoList taskList = new TodoList();

        String task = taskList.getTaskById(5);

        assertEquals("invalid input", task);
    }

    @Test
    public void getAllTodoListTest() {
        TodoList taskList = new TodoList();
        String expectedResult = "1. Go to church [DONE]\n2. Coding [NOT DONE]\n3. Assignment [DONE]";

        String allTaskList = taskList.getAllTodoList();

        assertEquals(expectedResult, allTaskList);
    }

    @Test
    public void markTaskToDoneTest() {
        TodoList taskList = new TodoList();
        String expectedResult = "2. Coding [DONE]";

        taskList.markTaskToDone(2);
        String actual = taskList.getTaskById(2);

        assertEquals(expectedResult, actual);
    }

    @Test
    public void getTaskByCategoryTest() {
        TodoList taskList = new TodoList();
        String expectedResult = "2. Coding [NOT DONE]\n3. Assignment [DONE]";

        String allTaskList = taskList.getTaskListByCategory("Kuliah");

        assertEquals(expectedResult, allTaskList);
    }

    @Test
    public void deleteTaskByIdTest() {
        TodoList taskList = new TodoList();
        String expectedResult = "2. Coding [NOT DONE]\n3. Assignment [DONE]";

        taskList.deleteTaskById(1);

        assertEquals(expectedResult, taskList.getAllTodoList());
    }
}